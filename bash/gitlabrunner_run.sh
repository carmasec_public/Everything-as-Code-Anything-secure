#!/bin/bash

$(docker run -d --name flask-runner -v /var/run/docker.sock:/var/run/docker.sock -v /srv/flask-runner/config:/etc/gitlab-runner --rm gitlab/gitlab-runner)
if [[ $(docker inspect -f '{{.State.Running}}' flask-runner) = "true" ]]; then
	#statements
	echo -e "\e[42m##### GitLab runner is container running correctly#####\e[0m"
else
	echo -e "\e[31mError: Gitlab runner container not running correctly\e[0m"
fi
