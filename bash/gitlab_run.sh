!#/bin/bash

echo "Start gitlab-ce container"
$(docker run --detach \
  --hostname gitlab.example.com \
  --publish 443:443 --publish 80:80 --publish 22:22 \
  --name gitlab \
  --restart always \
  --volume /srv/gitlab/config:/etc/gitlab \
  --volume /srv/gitlab/logs:/var/log/gitlab \
  --volume /srv/gitlab/data:/var/opt/gitlab \
  gitlab/gitlab-ce:latest)
echo "Check if GitLab container is running"
if [[ $(docker inspect -f '{{.State.Running}}' gitlab) = "true" ]]; then
	#statements
	echo -e "\e[42m##### GitLab container is running correctly#####\e[0m"
else
	echo -e "\e[31mError: Jenkins container is not running correctly\e[0m"
fi
