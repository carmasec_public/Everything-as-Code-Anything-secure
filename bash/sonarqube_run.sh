#!/bin/bash

$(docker run -d --name sonarqube --restart always -p 9000:9000 -p 9092:9092 sonarqube:latest)

if [[ $(docker inspect -f '{{.State.Running}}' sonarqube) = "true" ]]; then
	#statements
	echo -e "\e[42m##### Sonarqube container is running correctly#####\e[0m"
else
	echo -e "\e[31mError:Sonarqube container is not running correctly\e[0m"
fi