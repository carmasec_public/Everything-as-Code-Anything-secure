#!/bin/bash

IP = $(hostname -I)

echo "Please insert registration token"

read token

$(docker exec -it flask-runner gitlab-runner register -n --url http://$IP --registration-token $token --clone-url http://$IP --executor docker --docker-image "docker:latest" --docker-privileged)