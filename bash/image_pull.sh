#!/bin/bash

echo
echo "This script pulls necessary images from Dockerhub"
echo
# Display User
echo "Current User:" $(whoami)
if [[ $(whoami) != "docker" ]]; then
	echo "Switching to docker user for the next commands"
fi
$(docker pull gitlab/gitlab-ce)
$(docker pull gitlab/gitlab-runner)
$(docker pull sonarqube)