### Technische Dokumentation des Fachartikels

Setup und Konfiguration einer einfachen CI Security Pipeline

1. Vorbereitung
2. Ansible Playbook
3. Inventory Datei
4. Ansible Playbook ausführen
5. Bash Scripte
6. GitLab Runner
7. Sonarqube
8. .gitlab-ci.yml

#### 1. Vorbereitung

Server:
Diese Anleitung setzt ein installierte Version von Ubuntu Server 18.04 LTS voraus. 
Zu allererst sollte man den Port des Host Systems ändern um eine Problemlose Interaktion mit Gitlab über Port 22 zu gewährleisten.
Dazu mit dem Editor seiner Wahl die sshd_config Datei bearbeiten (/etc/ssh/sshd_config), den Port ändern und die Datei speichern und schließen. Danach muss der SSH Dienst neu gestartet werden:
```sh
sudo systemctl restart sshd.service
```
Außerdem muss der User in der sudoers File geändert werden, sodass keine Passwortabfrage stattfindet. Da Pakete installiert und User hinzugefügt werden, sind sudo Privilegien erforderlich. Dazu einfach mit
```sh
sudo visudo
```
die sudoers Datei öffnen und am Ende folgendes Einfügen:
```sh
user ALL=(ALL) NOPASSWD:ALL
```

User Maschine:
Auf dem eigenen Laptop muss Ansible installiert werden. Zudem ist es Sinnvoll ansible lint zu installieren. Ansible lint überprüft die Yaml Playbooks auf Syntaxfehler.
 ```sh
sudo apt install ansible ansible-lint
```

#### 2. Ansible Playbook
In der ersten Zeile des Ansible Playbooks wird der Host angegeben der zu Konfigurieren ist. Da in diesem Fall eine inventory Datei im gleichen Ordner wie das Ansible Playbook liegt und wir diese nachher im Kommando kann hier die definierte Host Gruppe angegeben werden. Die inventory Datei betrachten wir später etwas genauer.
Was macht das Script genau? <br>
Zunächst deinstalliert das Script die bereits Vorhandenen Docker Pakete der Ubuntu Installation. Diese wäre hinderlich bei der Installation von der Docker Community Edition. Danach werden die benötigten Pakete installierte sowie der GPG Schlüssel und das Docker Repository hinzugefügt. Daraufhin erfolgt die Installation von Docker CE. Diese Schritte orientieren sich an der Anleitung auf der Docker Homepage und kann dort ebenfalls nachgelesen werden. 
Da Docker einen eigenen User und eine eigene Gruppe benötigt wird als nächstes geprüft ob diese Gruppe vorhanden ist und ein Docker User angelegt und der Gruppe hinzugefügt, sowie die Rechte dieses Nutzers eskaliert. Es ist notwendig das dieser User in der sudoers Datei ist. Darüber hinaus stellen wir noch sicher das Docker automatisch bei Systemstart ausgeführt bzw. mit dem System direkt gestartet wird. <br>
Als nächstes beschäftigen wir uns mit der inventory Datei und dem anwenden des Playbooks auf den Server/Host.

#### 3. Inventory Datei

Die inventory Datei beinhaltet in diesem Fall eine einfache Gruppe, welche in den eckigen Klammern steht (in diesem Fall testserver):

```sh
[testserver]
HOST-IP ansible_python_interpreter=/usr/bin/python3 ansible_port=SSH-PORT
```
Darunter steht eine Exemplarische IP-Adresse des Hosts auf den wir das Playbook anwenden wollen. Dahinter wird der Port (SSH Port) definiert, da dieser ja nicht mehr auf dem SSH Standard Port 22 liegt sondern anfänglich geändert wurde. Darauf folgend ist ein Parameter angegeben welcher Ansible dazu auffordert als Standard Python Interpreter die Version 3 zu benutzen. Dies ist zum jetzigen Zeitpunkt noch notwendig sollte aber in Zukunft nicht weiter benötigt werden, da Python Version 2 veraltet ist und in Zukunft nicht mehr verwendet werden sollte und auch nicht weiter verwendet wird (Ubuntu Release 20.04).

#### 4. Ansible Playbook ausführen

Zunächst etablieren wir nun eine ssh Verbindung zu Host mit dem neu angegebenen Port um den ssh Schlüssel Fingerprint aus zu tauschen und in unserem System zu hinterlegen. Daraufhin wird das Nutzerpasswort des Hosts abgefragt. Nach dem erfolgreichen Login können Sie sich direkt wieder abmelden. Nun kann das Ansible Script ausgeführt werden:
```sh
ansible-playbook Name_Des_Playbooks -i inventory

PLAY RECAP ********************************************************************************** 
10.10.2.4                  : ok=11   changed=7    unreachable=0    failed=0
```
Am Ende sollte das Play Recap stehen und keine failed oder unreachable Tasks aufweisen. Damit ist Docker erfolgreich installiert.

#### 5. Bash Scripte
Die kopierten Bash Scripte könne nacheinander aufgerufen werden. Diese Scripte pullen die Images von Dockerhub und starten die jeweiligen Container. Im folgenden werden die Eigenschaften noch genauer erläutert. Einzige Einschränkung ist das der GitLab Container vollständig gestartet ist bevor der GitLab Runner gestartet wird. Dies hat praktische Gründe, da GitLab den Runner erkennt und man im Admin Bereich von GitLab diesen dann sehr schnell und einfach einbinden kann.
<br>
image_pull: Bezieht die erforderlichen Images von DockerHub (GitLab, GitLab Runner, SonarQube)
<br>
gitlab_run: Startet den GitLab Container und konfiguriert diesen
<br>
gitlabrunner_run: Startet den GitLab Runner
<br>
gitlabrunner_config: Ermittelt die Host IP und erwartet die Usereingabe des Tokens um den GitLab Runner einzurichten
<br>
sonarqube_run: Startet den SonarQube Server


#### 6. GitLab Runner
Der Gitlab Runner führt die Anweisungen aus die in der .gitlab-ci.yml Datei gespeicht wird. Der Runner sollte einem Projekt zugeordnet sein, damit dieser spezifisch die Aufgaben dieses Projekts ausführt. Diese Verknüpfung kann im Adminbereich eines Projektes innerhalb der WebUI erstellt werden.


#### 7. SonarQube
Hiermit ist nur der SonarQube Dashboard Server gemeint. Dieser Server nimmt die Ergebnisse des Scanners entgegen (dieser sollte innerhalb des Projektordners separat definiert werden). Diese Ergebnisse werden dann pro Projekt innerhalb der WebUI dargestellt. Der Vorteil dieser WebUI ist die Grafische Darstellung der Ergebnisse und der besseren nach Verfolgbarkeit des Projektfortschrittes. <br>
Beispiel sonar-project.properties Datei:

```sh

# Required metadata
sonar.projectKey=flask
sonar.projectName=flask

# Comma-separated paths to directories with sources (required)
sonar.sources=./

# Language
sonar.language=py
sonar.profile=node
# Encoding of sources files
sonar.sourceEncoding=UTF-8

```

#### 8. .gitlab-ci.yml

Im folgenden ist die Automatisierungsdatei zu sehen. Es müssen noch anpassungen vorgenommen werden. Zum einen ist der SONAR_TOKEN einzutragen, welcher sich über den SonarQube Server ermitteln lässt. Sowie muss unter SONAR_HOST_URL die IP Adresse des SonaQube Servers eingefügt werden (bitte Port beibehalten, da der Server über Docker so konfiguriert wurde).

```yaml
image:
  name: sonarsource/sonar-scanner-cli:latest
  entrypoint: [""]
variables:
  SONAR_TOKEN: "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
  SONAR_HOST_URL: "http://XXXXX:9000"
  GIT_DEPTH: 0
sonarqube-check:
  stage: test
  script:
    - sonar-scanner -Dsonar.qualitygate.wait=true
  allow_failure: true
  only:
    - merge_request
    - master
```

